const express = require('express');
const path = require('path');

const app = express();
const port = process.env.PORT || 8080;
app.use(express.static(path.join(__dirname, 'client/build')));

app.get('/ping', function (req, res) {
    return res.send('\"pong\"');
});

const options = {
    timeout: 10000, // timeout of 10s (5s is the default)

    retry: {
        initial: 100,
        multiplier: 2,
        max: 15e3
    }
};
const bgg = require('bgg')(options);

app.get('/api/user/:username', function (req, res) {
    bgg('user', {name: req.params.username})
        .then((results => {
            // console.log(results);
            // console.log(req.params.username);
            res.send(results);
        }));

});

/**
 * At the moment, this api call is fairly stupid. It will not respond
 * correctly to a 202 as the bgg API doesn't yet support the delaying
 * the BGG_XML_API2 imposes. The client side will have to deal with this
 * by waiting and sending a subsequent response.
 */
app.get('/api/collection/:username',function (req, res) {
    bgg('collection', {username: req.params.username})
        .then((results => {
            res.send(results);
        }));

});

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, 'client/build', 'index.html'));
});

app.listen(port, () => console.log(`Listening on port ${port}`));