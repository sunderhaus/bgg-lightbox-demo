import React, {Component} from 'react';

class Item extends Component {

    render() {
        return (
            <div>
                <a>
                    <img src={this.props.thumbnail}/>
                    <h3>{this.props.name.$t}</h3>
                </a>
            </div>
            )
    }
}

export default Item;
