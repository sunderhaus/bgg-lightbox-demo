import React, {Component} from 'react';

class User extends Component {

    render() {
        const classShowHide = this.props.show ? "user display-block" : "user display-none";
        let markup, message = null;

        if (this.props.totalItems > 0) {
            message = <h2>Below is a BGG Collection belonging to {this.props.user.firstname.value} {this.props.user.lastname.value} containing {this.props.totalItems} items!</h2>
        } else {
            message = <h2>The BGG Collection belonging to {this.props.user.firstname.value} {this.props.user.lastname.value} is empty.</h2>
        }

        if (this.props.show) {
            markup = (
                <div className={classShowHide}>
                    {message}
                </div>
            )
        }
        return markup;
    }

}

export default User;
