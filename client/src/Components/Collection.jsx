import React, {Component} from 'react';
import Item from "./Item";

class Collection extends Component {

    lightboxItem = (objectid) => {
        this.props.showLightboxForObject(objectid);
    };

    render = () => {
        let items = null;
        if (this.props.items.totalitems > 0) {
            items = this.props.items.item.map((item, index) => {
                return (
                    <li key={item.objectid} id={`item-${index}`} className="collection-item" onClick={this.lightboxItem.bind(null, item.objectid)}>
                        <Item {...item}/>
                    </li>
                );
            });
        }

        const classShowHide = this.props.show ? "collection display-block" : "collection display-none";
        let markup = null;
        if (this.props.show) {
            markup = (
                <div className={classShowHide}>
                    <ul className={"collection-list"}>
                        {items}
                    </ul>
                </div>
            )
        }
        return markup;
    }
}

export default Collection;
