import React, {Component} from 'react';

class Lightbox extends Component {

    componentDidMount() {
        document.addEventListener("keyup", (event) => {
            if (event.keyCode === 27) {
                this.props.hideModal()
            }
        })
    }

    render() {
        const classShowHide = this.props.show ? "lightbox display-block" : "lightbox display-none";
        let markup = null;
        if (this.props.show) {
            markup = (
                <div className={classShowHide}>
                    <div className="lightbox-inner">
                        <button className="close-button" onClick={this.props.hideModal}>x</button>
                        {this.props.children}
                    </div>
                </div>
            )
        }
        return markup;
    }

}

export default Lightbox;
