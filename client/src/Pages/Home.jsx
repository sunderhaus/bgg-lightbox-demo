import React, { Component } from 'react';
import Lightbox from "../Components/Lightbox";
import User from "../Components/User";
import Collection from "../Components/Collection";
import sunderhausUser from "../Fixtures/user.json";
import sunderhausCollection from "../Fixtures/collection.json";

class Home extends Component {
  state = {
      show: false,
      bggUsername: 'sunderhaus',
      bggUserData: sunderhausUser,
      bggUserCollectionData: sunderhausCollection,
      loadingUserData: false,
      loadingUserCollectionData: false
  };

    componentDidMount() {
        document.getElementById("usernameTextBox").addEventListener("keyup", (event) => {
            if (event.keyCode === 13) {
                this.requestBGGForUsername()
            }
        })
    }

  fetchBGGData() {
      this.setState({loadingUserData: true, loadingUserCollectionData: true}, () => {
        this.fetchBGGUserData();
        this.fetchBGGUserCollectionData();
      });
  };

  fetchBGGUserData() {
      fetch(`/api/user/${this.state.bggUsername}`)
      .then(response => {
          return response.json();
      })
      .then(json => {
          this.setState({
              bggUserData: json,
              loadingUserData: false
          });
      });
  };

  fetchBGGUserCollectionData() {
      fetch(`/api/collection/${this.state.bggUsername}`)
      .then(response => {
          return response.json();
      })
      .then(json => {
          this.setState({
              bggUserCollectionData: json,
              loadingUserCollectionData: false
          });
      });
  };

  requestBGGForUsername = () => {
      let submittedUsername = document.getElementById("usernameTextBox").value;
      this.setState({bggUsername: submittedUsername}, () => {this.fetchBGGData()});
  };

  showLightboxForObject = (objectid) => {
      const choice = this.state.bggUserCollectionData.items.item.find(i => {
          return i.objectid === objectid;
      });

      this.setState({selectedItem: choice}, this.showModal);
  };

  showModal = () => {
      this.setState({ show: true }, () => {
          // Prevent scrolling
          document.body.classList.add("overflow-hidden");
      });
  };

  hideModal = () => {
      this.setState({ show: false}, () => {
          // Enable scrolling
          document.body.classList.remove("overflow-hidden");
      });
  };

  isEmpty = (obj) => {
    for (var prop in obj) {
        if (obj.hasOwnProperty(prop))
            return false;
    }
    return JSON.stringify(obj) === JSON.stringify({});
  };

  render() {
      let loadingMarkup, userMarkup, collectionMarkup, invalidMessage, lightbox = null;
      let loadingData = this.state.loadingUserData || this.state.loadingUserCollectionData;
      if (loadingData) {
          loadingMarkup = (
              <h1>LOADING!</h1>
          );
      } else {
          /*
            There are a number of circumstances where data won't load from boardgamegeek.com. These do their best to safeguard.
          */
          if (this.state.bggUserCollectionData.message === "Your request for this collection has been accepted and will be processed.  Please try again later for access.") {
              invalidMessage = <h2>We're waiting for this collection to load on BGG. Please try again in a moment.</h2>
          } else if (this.state.bggUserData.user.id === "" || this.state.bggUserCollectionData.hasOwnProperty("errors")) {
              invalidMessage = <h2>It appears {this.state.bggUsername} isn't a valid user. Try another one above.</h2>
          } else {
              userMarkup = (<User show={!loadingData} {...this.state.bggUserData} totalItems={this.state.bggUserCollectionData.items.totalitems} />);
              collectionMarkup = (<Collection show={!loadingData} showLightboxForObject={this.showLightboxForObject} {...this.state.bggUserCollectionData} />);
          }

          if (this.state.show) {
              let i = this.state.selectedItem;
              lightbox = (
                  <Lightbox show={this.state.show} hideModal={this.hideModal}>
                      <img src={i.image}/>
                      <h1>{i.name.$t}</h1>
                      <ul>
                          <li>Published: {i.yearpublished}</li>
                          <li>Type: {i.subtype}</li>
                          <li>BGG Collection ID: {i.collid}</li>
                      </ul>
                  </Lightbox>
              );
          }
      }

      return (
          <div>
              <h1>Welcome, fellow geek!</h1>
              <p>The text box below can be used to get details from BoardGameGeek.com. Enter a username to view a User
                  and their collection details</p>
              <input id="usernameTextBox" type="text" placeholder="Enter a boardgamegeek.com username"/>
              <button type="button" onClick={this.requestBGGForUsername}>Submit</button>
              {loadingMarkup}
              <hr/>
              {invalidMessage}
              {userMarkup}
              {collectionMarkup}
              {lightbox}
          </div>
      )
  }
}

export default Home;
